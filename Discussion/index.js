require('dotenv').config()
const e = require('express')
const express = require('express')
const mongoose = require('mongoose')

const app = express()
const port = 4000

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("Connected to MongoDB Atlas!"))

app.use(express.json())
app.use(express.urlencoded({extended: true}))


// [SECTION] Mongoose Schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema)

// Creating a new task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
// app.post('/tasks', (req, res) => {
//     const { name } = req.body
//     // To check if there are duplicate tasks
//     Task.findOne({name}, (err, result) => {
//         if (result != null && result.name == name) {
//             return res.send('Duplicate task found!')
//         } else {
//             let newTask = new Task({
//                 name
//             })
//             newTask.save((saveErr, savedTask) => {
//                 if (saveErr) {
//                     return console.error(saveErr)
//                 } else {
//                     return res.status(201).send("New task created")
//                 }
//             })
//         }
//     })
// })

app.post('/tasks', async (req, res) => {
    const { name } = req.body
    // To check if there are duplicate tasks
    const duplicate = await Task.findOne({ name })
    if (duplicate) {
        return res.status(400).json({ message: 'Task already exists' })
    }

    const newTask = await Task.create({ name })
    res.status(201).json({message: 'New task created'})
})

app.get('/tasks', async (req, res) => {
    const tasks = await Task.find()
    if (!tasks.length) {
        return res.status(404).json({ message: 'No tasks found' })
    }
    res.status(200).json(tasks)
})






app.listen(port, () => console.log(`Server is up and running on port ${port}`))