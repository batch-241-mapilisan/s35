require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')

const app = express()
const port = 4000

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error'))
db.once('open', () => console.log('Connected to MongoDB Atlas!'))

app.use(express.json())
app.use(express.urlencoded({extended: true}))

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

const User = mongoose.model("User", userSchema)

// POST
// Create new user
app.post('/signup', async (req, res) => {
    const { username, password } = req.body

    // Confirm data
    if (!username || !password) {
        return res.status(400).json({ message: 'All fields are required' })
    }

    // Check for duplicate
    const duplicate = await User.findOne({ username })

    if (duplicate) {
        return res.status(409).json({ message: 'User already exist '})
    }

    // Store user information
    const userObject = {
        username,
        password
    }

    // Create and store new user
    const newUser = await User.create(userObject)

    if (newUser) {
        res.status(201).json({ message: `New user ${username} created` })
    } else {
        res.status(400).json({ message: 'Invalid user data received' })
    }
})

// GET
// Get all users
app.get('/users', async (req, res) => {
    const users = await User.find()

    if (!users.length) {
        return res.status(404).json({ message: 'No users found' })
    }
    res.status(200).json(users)
})

app.listen(port, () => console.log(`Server is up and running on port ${port}`))
